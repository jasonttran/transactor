#include "heartbeat.hpp"

#include <stdbool.h>
#include "FreeRTOS.h"
#include "task.h"

#include "gpio.hpp"


void HEARTBEAT_task(void *parameters)
{
    Gpio led(GPIO_PORT_C, 5);
    led.config_output();
    led.set_high();

    while (true) {
        led.toggle();
        vTaskDelay(500);
    }
}
