#include <stdbool.h>
#include <stdint.h>

#include "adc0.hpp"
#include "can0.hpp"
#include "nvic.h"
#include "spi0.hpp"
#include "system.hpp"
#include "uart0.hpp"

#define CORE_IVT_SIZE 16U
#define DEVICE_IVT_SIZE 95U
#define IVT_SIZE (CORE_IVT_SIZE + DEVICE_IVT_SIZE)

extern "C"
{
    extern void (* const core_interrupt_vector_table[IVT_SIZE])(void);
    extern void _estack(void);
    extern void _start(void);
    extern void vPortSVCHandler(void);
    extern void xPortPendSVHandler(void);
    extern void xPortSysTickHandler(void);
}

static void halt(void);

__attribute__ ((section(".core_interrupt_vector_table")))
void (* const core_interrupt_vector_table[IVT_SIZE])(void) = {
    // Core interrupt vectors
    _estack,  // 0 ARM: Initial stack pointer
    _start,  // 1 ARM: Initial program counter
    halt,  // 2 ARM: Non-maskable interrupt
    halt,  // 3 ARM: Hard fault
    halt,  // 4 ARM: Memory management fault
    halt,  // 5 ARM: Bus fault
    halt,  // 6 ARM: Usage fault
    halt,  // 7 ARM: Reserved
    halt,  // 8 ARM: Reserved
    halt,  // 9 ARM: Reserved
    halt,  // 10 ARM: Reserved
    vPortSVCHandler,  // 11 ARM: Supervisor call (SVCall)
    halt,  // 12 ARM: Debug monitor
    halt,  // 13 ARM: Reserved
    xPortPendSVHandler, // 14 ARM: Pendable request for system service (PendableSrvReq)
    xPortSysTickHandler, // 15 ARM: System Tick Timer (SysTick)

    // Device interrupt vectors
    halt,  // 0
    halt,  // 1
    halt,  // 2
    halt,  // 3
    halt,  // 4
    halt,  // 5
    halt,  // 6
    halt,  // 7
    halt,  // 8
    halt,  // 9
    halt,  // 10
    halt,  // 11
    halt,  // 12
    halt,  // 13
    halt,  // 14
    halt,  // 15
    halt,  // 16
    halt,  // 17
    halt,  // 18
    halt,  // 19
    halt,  // 20
    halt,  // 21
    halt,  // 22
    halt,  // 23
    halt,  // 24
    halt,  // 25
    Spi0::isr_transaction_hook,  // 26
    halt,  // 27
    halt,  // 28
    Can0::isr_transaction_hook,  // 29
    halt,  // 30
    halt,  // 31
    halt,  // 32
    halt,  // 33
    halt,  // 34
    halt,  // 35
    halt,  // 36
    halt,  // 37
    halt,  // 38
    halt,  // 39
    halt,  // 40
    halt,  // 41
    halt,  // 42
    halt,  // 43
    halt,  // 44
    Uart0::isr_transaction_hook,  // 45
    halt,  // 46
    halt,  // 47
    halt,  // 48
    halt,  // 49
    halt,  // 50
    halt,  // 51
    halt,  // 52
    halt,  // 53
    halt,  // 54
    halt,  // 55
    halt,  // 56
    Adc0::isr_handle_hook,  // 57
    halt,  // 58
    halt,  // 59
    halt,  // 60
};


static void halt(void)
{
    (void)core_interrupt_vector_table;  // Resolve symbol not used compiler warning
    SYSTEM_error_loop();
}
