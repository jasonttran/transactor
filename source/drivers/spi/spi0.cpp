#include "spi0.hpp"

#include "mk20d7.h"
#include "system.hpp"


Spi0::Spi0() : SpiBase()
{

}


Spi0 * Spi0::get_instance(void)
{
    static Spi0 *instance = NULL;
    if (instance == NULL) {
        instance = new Spi0;
    }
    return instance;
}


void Spi0::isr_transaction_hook(void)
{
    Spi0 *spi0 = Spi0::get_instance();
    spi0->isr_handle_transaction();
}


void Spi0::initialize_peripheral(void)
{
    SIM->SCGC6 |= SIM_SCGC6_SPI0_MASK;

    PORTC->PCR[6] |= PORT_PCR_MUX(2);  // SPI0 MOSI
    PORTC->PCR[7] |= PORT_PCR_MUX(2);  // SPI0 MISO
    PORTD->PCR[1] |= PORT_PCR_MUX(2);  // SPI0 SCK

}


SPI_Type * const Spi0::get_spi_base_ptr(void)
{
    return SPI0;
}


uint32_t Spi0::get_peripheral_clock_hz(void)
{
    return SYSTEM_get_perpherial_clock_mhz();
}


IRQn_Type Spi0::get_interrupt_request_number(void)
{
    return SPI0_IRQn;
}
