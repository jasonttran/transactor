#include "spi_base.hpp"

#include "gpio.hpp"

    /*
     * SCK = (PCLK / (PBR * BR))
     * 
     * Where:
     * Serial clock (SCK)
     * Prescaler (PBR)
     * Scaler (BR)
     * 
     * (PBR * BR) = PCLK / SCK
     * BR = PCLK / (SCK * PBR)
     *
     * If:
     * PBR = 7
     * 
     * Therefore:
     * BR = PCLK / (2* SCK)
     */

/*
 * SCK baud rate = (fSYS/PBR) x [(1+DBR)/BR]
 */

#define SCK_PRESCALAR 7U
#define SCK_SCALAR(pclk, sck) ((pclk) / (SCK_PRESCALAR * (sck)))
#define SCK_SCALAR_REG(scalar) (((scalar) / 2U) - 1U)

#define QUEUE_SIZE 1U


SpiBase::SpiBase()
{

}


void SpiBase::initialize(uint32_t serial_clock_hz)
{
    this->spi_base_ptr = get_spi_base_ptr();
    this->interrupt_request_number = get_interrupt_request_number();
    this->receive_queue = xQueueCreate(QUEUE_SIZE, sizeof(uint8_t));
    this->lock = xSemaphoreCreateMutex();

    initialize_peripheral();

    this->spi_base_ptr->MCR |= (SPI_MCR_HALT_MASK | SPI_MCR_MDIS_MASK);  // Halt AND disable SPI controller

    this->spi_base_ptr->MCR |= SPI_MCR_MSTR_MASK;  // Enable master mode
    this->spi_base_ptr->MCR &= ~(SPI_MCR_CONT_SCKE_MASK);  // Disable continuous serial clock (SCK)
    this->spi_base_ptr->MCR |= SPI_MCR_CLR_TXF_MASK;  // Clear transmit FIFO
    this->spi_base_ptr->MCR |= SPI_MCR_CLR_RXF_MASK;  // Clear receive FIFO

    // SPI clock and transfer attribute configuration - SCK configuration
    uint32_t brscaler = 0U;
    uint32_t rate = (get_peripheral_clock_hz() / serial_clock_hz) / 4U;       // assumes baud prescaler of / 4
    uint32_t final = (get_peripheral_clock_hz() / 4U);
    uint32_t ctar = 0U;
    if (rate < 2) {
        rate = rate / 2;
        final = final / 2;
        ctar = SPI_CTAR_DBR_MASK;
    }
    brscaler = 1;
    while ((1U << brscaler) < rate) {
        brscaler++;
    }
    if (brscaler > 15) {
        return;  // if out of range, give up; no SPI
    }
    ctar |= brscaler;  // merge baud-rate doubler (if used) with scaler
    ctar |= SPI_CTAR_FMSZ(7);  // 8 bit data frame
    this->spi_base_ptr->CTAR[0] = ctar;

    this->spi_base_ptr->RSER |= SPI_RSER_TCF_RE_MASK;  // Enable transmission complete interrupt
    this->initialize_interrupt();

    this->spi_base_ptr->MCR = SPI_MCR_MSTR_MASK;  // Enable SPI controller; master mode
}


uint8_t SpiBase::exchange_byte(uint8_t byte, Gpio chip_select)
{
    uint8_t data = 0U;
    xSemaphoreTake(this->lock, portMAX_DELAY);
    chip_select.set_low();
    this->spi_base_ptr->PUSHR = (byte << SPI_PUSHR_TXDATA_SHIFT);
    xQueueReceive(this->receive_queue, &data, portMAX_DELAY);
    chip_select.set_high();
    xSemaphoreGive(this->lock);
    return data;
}


uint8_t SpiBase::exchange_bytes(uint8_t *src_bytes, uint8_t *dest_bytes, uint8_t size, Gpio chip_select)
{
    xSemaphoreTake(this->lock, portMAX_DELAY);
    chip_select.set_low();
    for (uint8_t index = 0U; index < size; index++) {
        this->spi_base_ptr->PUSHR = (src_bytes[index] << SPI_PUSHR_TXDATA_SHIFT);
        xQueueReceive(this->receive_queue, &dest_bytes[index], portMAX_DELAY);
    }
    chip_select.set_high();
    xSemaphoreGive(this->lock);
    return size;
}


void SpiBase::isr_handle_transaction(void)
{
    uint8_t data = 0U;
    if (this->spi_base_ptr->SR & SPI_SR_TCF_MASK) {
        data = this->spi_base_ptr->POPR & SPI_POPR_RXDATA_MASK;
        this->spi_base_ptr->SR |= SPI_SR_TCF_MASK;  // Acknowledge and clear transmit complete flag
    }
    xQueueSendFromISR(this->receive_queue, &data, NULL);
    NVIC_clear_interrupt_request(this->interrupt_request_number);
}


/*
 * Private methods
 */


void SpiBase::initialize_interrupt(void)
{
    NVIC_config_interrupt_request_priority(this->interrupt_request_number, DEFAULT_INTERRUPT_PRIORITY_LEVEL);
    NVIC_enable_interrupt_request(this->interrupt_request_number);
}
