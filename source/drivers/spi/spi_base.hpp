#ifndef SPI_BASE_HPP
#define SPI_BASE_HPP


#include <stdbool.h>
#include <stdint.h>
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

#include "gpio.hpp"
#include "mk20d7.h"
#include "nvic.h"


class SpiBase
{
public:
    void initialize(uint32_t serial_clock_hz);
    uint8_t exchange_byte(uint8_t byte, Gpio chip_select);
    uint8_t exchange_bytes(uint8_t *src_bytes, uint8_t *dest_bytes, uint8_t size, Gpio chip_select);

    /* Interrupts */
    void isr_handle_transaction(void);

protected:
    SpiBase();

private:
    virtual void initialize_peripheral(void) = 0;
    virtual SPI_Type * const get_spi_base_ptr(void) = 0;
    virtual uint32_t get_peripheral_clock_hz(void) = 0;
    virtual IRQn_Type get_interrupt_request_number(void) = 0;

    void initialize_interrupt(void);

    SPI_Type *spi_base_ptr;
    IRQn_Type interrupt_request_number;
    QueueHandle_t receive_queue;
    SemaphoreHandle_t lock;
};


#endif  // SPI_BASE_HPP
