#ifndef SPI0_HPP
#define SPI0_HPP

#include "spi_base.hpp"


class Spi0 : public SpiBase
{
public:
    static Spi0 * get_instance(void);
    static void isr_transaction_hook(void);

protected:
    Spi0();

private:
    virtual void initialize_peripheral(void);
    virtual SPI_Type * const get_spi_base_ptr(void);
    virtual uint32_t get_peripheral_clock_hz(void);
    virtual IRQn_Type get_interrupt_request_number(void);
};


#endif  // SPI0_HPP
