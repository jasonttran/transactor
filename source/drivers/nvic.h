#ifndef NVIC_H
#define NVIC_H

#include "mk20d7.h"

#define DEFAULT_INTERRUPT_PRIORITY_LEVEL 3U

#ifdef __cplusplus
extern "C" {
#endif  // __cplusplus


void NVIC_enable_interrupt_request(IRQn_Type request_number);
void NVIC_disable_interrupt_request(IRQn_Type request_number);
void NVIC_clear_interrupt_request(IRQn_Type request_number);
void NVIC_config_interrupt_request_priority(IRQn_Type request_number, uint8_t priority_level);
void NVIC_reset(void);


#ifdef __cplusplus
}
#endif  // __cplusplus

#endif  // NVIC_H
