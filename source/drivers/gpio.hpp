#ifndef GPIO_HPP
#define GPIO_HPP

#include "mk20d7.h"

#include <stdbool.h>
#include <stdint.h>

typedef enum {
    GPIO_PORT_A=0,
    GPIO_PORT_B,
    GPIO_PORT_C,
    GPIO_PORT_D,
    GPIO_PORT_E,
} GpioPort_E;


class Gpio
{
public:
    Gpio(GpioPort_E port, uint8_t pin);

    void config_output(void);
    void config_input(void);
    void set_high(void);
    void set_low(void);
    void toggle(void);
    bool read(void);

private:
    const uint8_t pin;
    volatile GPIO_Type *gpio_base_ptr;
};


#endif  // GPIO_HPP
