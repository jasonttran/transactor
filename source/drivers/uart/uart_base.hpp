#ifndef UART_BASE_HPP
#define UART_BASE_HPP

#include <stdbool.h>
#include <stdint.h>
#include "FreeRTOS.h"
#include "queue.h"
#include "semphr.h"

#include "mk20d7.h"
#include "nvic.h"


class UartBase
{
public:
    void initialize(uint32_t baud, uint32_t send_timeout, uint32_t receive_timeout);
    bool send(uint8_t data);
    bool send_buffer(uint8_t *buffer, uint32_t length);
    bool receive(uint8_t *data);
    bool receive_buffer(uint8_t *buffer, uint32_t length);
    bool is_receive_ready(void);
    bool is_send_ready(void);

    /* Interrupts */
    void isr_handle_transaction(void);

protected:
    UartBase();

private:
    virtual void initialize_peripheral(void) = 0;
    virtual UART_Type * const get_uart_base_ptr(void) = 0;
    virtual uint32_t get_peripheral_clock_hz(void) = 0;
    virtual IRQn_Type get_interrupt_request_number(void) = 0;

    void initialize_interrupt(void);
    void isr_finalize_send(void);
    void isr_enqueue_receive(void);

    uint32_t send_timeout;
    uint32_t receive_timeout;
    UART_Type *uart_base_ptr;
    IRQn_Type interrupt_request_number;
    QueueHandle_t receive_queue;
    SemaphoreHandle_t send_semaphore;
    SemaphoreHandle_t lock;
};


#endif  // UART_BASE_HPP
