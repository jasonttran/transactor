#include "uart0.hpp"

#include <stdlib.h>
#include "FreeRTOS.h"
#include "task.h"

#include "gpio.hpp"
#include "mk20d7.h"
#include "nvic.h"
#include "system.hpp"


Uart0::Uart0() : UartBase()
{

}


Uart0 * Uart0::get_instance(void)
{
    static Uart0 *instance = NULL;
    if (instance == NULL) {
        instance = new Uart0;
    }
    return instance;
}


void Uart0::isr_transaction_hook(void)
{
    Uart0 *uart0 = Uart0::get_instance();
    uart0->isr_handle_transaction();
}


void Uart0::initialize_peripheral(void)
{
    SIM->SCGC4 |= SIM_SCGC4_UART0_MASK;

    PORTB->PCR[16] |= PORT_PCR_PS_MASK;
    PORTB->PCR[16] |= PORT_PCR_PE_MASK;
    PORTB->PCR[16] |= PORT_PCR_MUX(3);

    PORTB->PCR[17] |= PORT_PCR_PS_MASK;
    PORTB->PCR[17] |= PORT_PCR_PE_MASK;
    PORTB->PCR[17] |= PORT_PCR_MUX(3);
}


UART_Type * const Uart0::get_uart_base_ptr(void)
{
    return UART0;
}


uint32_t Uart0::get_peripheral_clock_hz(void)
{
    return SYSTEM_get_system_clock_mhz();
}


IRQn_Type Uart0::get_interrupt_request_number(void)
{
    return UART0_RX_TX_IRQn;
}
