#ifndef UART0_HPP
#define UART0_HPP

#include "uart_base.hpp"


class Uart0 : public UartBase
{
public:
    static Uart0 * get_instance(void);
    static void isr_transaction_hook(void);

protected:
    Uart0();

private:
    virtual void initialize_peripheral(void);
    virtual UART_Type * const get_uart_base_ptr(void);
    virtual uint32_t get_peripheral_clock_hz(void);
    virtual IRQn_Type get_interrupt_request_number(void);
};


#endif  // UART0_HPP
