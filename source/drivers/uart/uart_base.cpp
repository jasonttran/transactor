#include "uart_base.hpp"

#include "mk20d7.h"

#define BAUDRATE_BITS(baud, clock_hz) ((clock_hz) / (baud * 16U))
#define BAUDRATE_FINE_ADJUSTMENT_BITS(baud, clock_hz, baudrate_bits) (((clock_hz * 32000U) / (baud * 16U)) - (baudrate_bits * 32))

#define DEFAULT_RECEIVE_QUEUE_SIZE 32U


UartBase::UartBase()
{

}


void UartBase::initialize(uint32_t baud, uint32_t send_timeout, uint32_t receive_timeout)
{
    this->send_timeout = send_timeout;
    this->receive_timeout = receive_timeout;
    this->receive_queue = xQueueCreate(DEFAULT_RECEIVE_QUEUE_SIZE, sizeof(uint8_t));
    this->send_semaphore = xSemaphoreCreateBinary();
    this->lock = xSemaphoreCreateMutex();

    this->uart_base_ptr = get_uart_base_ptr();
    this->interrupt_request_number = get_interrupt_request_number();
    uint32_t perpherial_clock_hz = get_peripheral_clock_hz();
    uint16_t baudrate_bits = BAUDRATE_BITS(baud, perpherial_clock_hz);

    initialize_peripheral();

    this->uart_base_ptr->C2 &= ~(UART_C2_TE_MASK);
    this->uart_base_ptr->C2 &= ~(UART_C2_RE_MASK);

    // Reset UART frame configuration
    this->uart_base_ptr->C1 = 0x00;

    this->uart_base_ptr->PFIFO &= ~(UART_PFIFO_TXFE_MASK);  // Disable transmit hardware FIFO
    this->uart_base_ptr->PFIFO &= ~(UART_PFIFO_RXFE_MASK);  // Disable receive hardware FIFO

    this->uart_base_ptr->BDH |= UART_BDH_SBR((baudrate_bits >> 8U));
    this->uart_base_ptr->BDL |= UART_BDL_SBR(baudrate_bits);
    this->uart_base_ptr->C4 |= UART_C4_BRFA(BAUDRATE_FINE_ADJUSTMENT_BITS(baud, perpherial_clock_hz, baudrate_bits));

    uart_base_ptr->C2 |= UART_C2_RIE_MASK;  // Enable receive interrupt

    this->initialize_interrupt();

    uart_base_ptr->C2 |= UART_C2_TE_MASK;
    uart_base_ptr->C2 |= UART_C2_RE_MASK;
}


bool UartBase::send(uint8_t data)
{
    xSemaphoreTake(this->lock, portMAX_DELAY);
    this->uart_base_ptr->D = data;
    this->uart_base_ptr->C2 |= UART_C2_TIE_MASK;  // Enable transmit interrupt
    bool success = xSemaphoreTake(this->send_semaphore, this->send_timeout);
    xSemaphoreGive(this->lock);
    return success;
}


bool UartBase::send_buffer(uint8_t *data, uint32_t length)
{
    bool success = true;
    xSemaphoreTake(this->lock, portMAX_DELAY);
    for (uint8_t index=0U; index < length; index++) {
        this->uart_base_ptr->D = data[index];
        this->uart_base_ptr->C2 |= UART_C2_TIE_MASK;  // Enable transmit interrupt
        success = xSemaphoreTake(this->send_semaphore, this->send_timeout);
        if (!success) {
            break;
        }
    }
    xSemaphoreGive(this->lock);
    return success;
}


bool UartBase::receive(uint8_t *data)
{
    bool success = false;
    uint8_t received_data = 0;
    if (data != NULL) {
        success = xQueueReceive(this->receive_queue, &received_data, this->receive_timeout);
        if (success) {
            *data = received_data;
        }
        else {
            *data = 0;
        }
    }
    return success;
}


bool UartBase::receive_buffer(uint8_t *data, uint32_t length)
{
    bool success = true;
    for (uint8_t index=0U; index < length; index++) {
        success = this->receive(&data[index]);
        if (!success) {
            break;
        }
    }
    return success;
}


bool UartBase::is_receive_ready()
{
    return (!(this->uart_base_ptr->S1 & UART_S1_RDRF_MASK));  // RX data register is not full
}


bool UartBase::is_send_ready()
{
    return (this->uart_base_ptr->S1 & UART_S1_TC_MASK);  // TX data register is empty
}


void UartBase::isr_handle_transaction(void)
{
    uint8_t status = this->uart_base_ptr->S1;

    if (status & UART_S1_RDRF_MASK) {
        this->isr_enqueue_receive();
    }
    else if (status & UART_S1_TDRE_MASK) {
        this->isr_finalize_send();
    }
    else {
        // Pass
    }

    NVIC_clear_interrupt_request(this->interrupt_request_number);
}


/*
 * Private methods
 */


void UartBase::initialize_interrupt(void)
{
    NVIC_config_interrupt_request_priority(this->interrupt_request_number, DEFAULT_INTERRUPT_PRIORITY_LEVEL);
    NVIC_enable_interrupt_request(this->interrupt_request_number);
}


void UartBase::isr_finalize_send(void)
{
    this->uart_base_ptr->C2 &= ~(UART_C2_TIE_MASK);  // Disable transmit interrupt
    (void)xSemaphoreGiveFromISR(this->send_semaphore, NULL);
}


void UartBase::isr_enqueue_receive(void)
{
    uint8_t data = this->uart_base_ptr->D;
    (void)xQueueSendFromISR(this->receive_queue, &data, NULL);
}
