#ifndef ADC_BASE_HPP
#define ADC_BASE_HPP

#include <stdbool.h>
#include <stdint.h>
#include "FreeRTOS.h"
#include "queue.h"
#include "semphr.h"

#include "mk20d7.h"
#include "nvic.h"

typedef enum {
    ADC_MODULE_0=0,
    ADC_MODULE_1,
    ADC_NUM_MODULES,
} AdcModule_E;


class AdcBase
{
public:
    bool initialize(void);
    uint16_t read(AdcModule_E module, uint8_t channel);

    /* Interrupts */
    void isr_handle(void);

protected:
    AdcBase();

private:
    virtual void initialize_peripheral(void) = 0;
    virtual bool initialize_pin(AdcModule_E module, uint8_t channel) = 0;
    virtual ADC_Type * get_adc_base_ptr(void) = 0;
    virtual IRQn_Type get_interrupt_request_number(void) = 0;

    void initialize_configuration(void);
    bool initialize_calibration(void);
    void initialize_interrupt(void);
    void enable_interrupt(void);
    void disable_interrupt(void);
    bool select_module(AdcModule_E module);

    ADC_Type *adc_base_ptr;
    IRQn_Type interrupt_request_number;
    QueueHandle_t result_queue;
    SemaphoreHandle_t lock;
    AdcModule_E selected_module;
};


#endif  // ADC_BASE_HPP
