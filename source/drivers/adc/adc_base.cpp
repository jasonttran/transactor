#include "adc_base.hpp"

#define ADC_NUM_CHANNELS_MAX 30  // Number of ADC channels per module


AdcBase::AdcBase()
{

}


bool AdcBase::initialize(void)
{
    bool success = false;
    this->adc_base_ptr = get_adc_base_ptr();
    this->interrupt_request_number = get_interrupt_request_number();
    this->result_queue = xQueueCreate(ADC_NUM_CHANNELS_MAX, sizeof(uint16_t));
    this->lock = xSemaphoreCreateMutex();
    this->selected_module = ADC_MODULE_0;

    initialize_peripheral();
    this->disable_interrupt();
    this->initialize_configuration();
    this->initialize_interrupt();

    return success;
}


uint16_t AdcBase::read(AdcModule_E module, uint8_t channel)
{
    uint16_t data = 0U;

    if (initialize_pin(module, channel)) {
        if (xSemaphoreTake(this->lock, portMAX_DELAY)) {
            (void)this->select_module(module);

            uint32_t status_control_a = this->adc_base_ptr->SC1[this->selected_module];
            status_control_a |= (ADC_SC1_ADCH_MASK);
            status_control_a &= (~(ADC_SC1_ADCH_MASK) | ADC_SC1_ADCH(channel));
            this->adc_base_ptr->SC1[this->selected_module] = status_control_a;
            this->enable_interrupt();
            if (xQueueReceive(this->result_queue, &data, portMAX_DELAY)) {
                this->disable_interrupt();
            }
            xSemaphoreGive(this->lock);
        }
    }

    return data;
}


void AdcBase::isr_handle(void)
{
    uint16_t data = this->adc_base_ptr->R[this->selected_module];
    (void)xQueueSendFromISR(this->result_queue, &data, NULL);
    NVIC_clear_interrupt_request(this->interrupt_request_number);
}


/*
 * Private methods
 */


void AdcBase::initialize_configuration(void)
{
    for (int32_t module = (int32_t)ADC_MODULE_0; module < (int32_t)ADC_NUM_MODULES; module++) {
        this->adc_base_ptr->SC1[module] |= ADC_SC1_ADCH(31);  // Select ADC channel 31 (equivalent to disabling ADC module)
        this->adc_base_ptr->SC1[module] &= ~(ADC_SC1_AIEN_MASK);  // Disable ADC interrupt
        this->adc_base_ptr->SC1[module] &= ~(ADC_SC1_DIFF_MASK);  // Configure single ended conversion
    }

    this->adc_base_ptr->CFG1 = 0;
    this->adc_base_ptr->CFG1 |= ADC_CFG1_ADIV(2);  // PCLK / DIV where DIV=4
    this->adc_base_ptr->CFG1 |= ADC_CFG1_ADLSMP_MASK;  // Long sample time
    this->adc_base_ptr->CFG1 |= ADC_CFG1_MODE(3);  // 16 bit conversion
    this->adc_base_ptr->CFG1 |= ADC_CFG1_ADICLK(0);  // Use bus clock

    this->adc_base_ptr->CFG2 = 0;
    this->adc_base_ptr->CFG2 &= ~(ADC_CFG2_MUXSEL_MASK);  // Select ADC channel A MUX
    this->adc_base_ptr->CFG2 |= ADC_CFG2_ADHSC_MASK;  // Enable high-speed conversion

    this->adc_base_ptr->CV1 &= ~(ADC_CV1_CV_MASK);
    this->adc_base_ptr->CV2 &= ~(ADC_CV2_CV_MASK);

    this->adc_base_ptr->SC2 = 0;
    this->adc_base_ptr->SC2 &= ~(ADC_SC2_ADTRG_MASK);  // Select software trigger

    this->adc_base_ptr->SC3 = 0;
    this->adc_base_ptr->SC3 &= ~(ADC_SC3_ADCO_MASK);  // Configure single conversion mode
    // this->adc_base_ptr->SC3 |= ADC_SC3_ADCO_MASK;  // Configure continuous conversion mode
    this->adc_base_ptr->SC3 |= ADC_SC3_AVGE_MASK;  // Enable hardware average function
    this->adc_base_ptr->SC3 |= ADC_SC3_AVGS(3);  // Configure 32 samples per average calculation

    this->adc_base_ptr->PGA = 0;

    for (int32_t module = (int32_t)ADC_MODULE_0; module < (int32_t)ADC_NUM_MODULES; module++) {
        this->adc_base_ptr->SC1[module] |= ADC_SC1_AIEN_MASK;  // Enable ADC interrupt
    }
}


bool AdcBase::initialize_calibration(void)
{
    bool success = false;
    this->adc_base_ptr->SC3 |= ADC_SC3_CAL_MASK;  // Activate calibration procedure
    while (this->adc_base_ptr->SC3 & ADC_SC3_CAL_MASK);  // Poll calibration flag until procedure completes
    success = (!(this->adc_base_ptr->SC3 & ADC_SC3_CALF_MASK));  // Check calibration failed bit for result; 0 if success

    if (success) {  // Finalize calibration
        // Perform final calibration procedure for plus side
        uint16_t calibration = 0U;
        calibration += this->adc_base_ptr->CLP0;
        calibration += this->adc_base_ptr->CLP1;
        calibration += this->adc_base_ptr->CLP2;
        calibration += this->adc_base_ptr->CLP3;
        calibration += this->adc_base_ptr->CLP4;
        calibration += this->adc_base_ptr->CLPS;
        calibration /= 2;
        calibration |= (1 << ((sizeof(uint16_t)*8)-1));
        this->adc_base_ptr->PG = calibration;

        // Repeat procedure for minus side
        calibration = 0U;
        calibration += this->adc_base_ptr->CLM0;
        calibration += this->adc_base_ptr->CLM1;
        calibration += this->adc_base_ptr->CLM2;
        calibration += this->adc_base_ptr->CLM3;
        calibration += this->adc_base_ptr->CLM4;
        calibration += this->adc_base_ptr->CLMS;
        calibration /= 2;
        calibration |= (1 << ((sizeof(uint16_t)*8)-1));
        this->adc_base_ptr->MG = calibration;
    }

    return success;
}


void AdcBase::initialize_interrupt(void)
{
    NVIC_config_interrupt_request_priority(this->interrupt_request_number, DEFAULT_INTERRUPT_PRIORITY_LEVEL);
    NVIC_enable_interrupt_request(this->interrupt_request_number);
}


void AdcBase::enable_interrupt(void)
{
    NVIC_enable_interrupt_request(this->interrupt_request_number);
}


void AdcBase::disable_interrupt(void)
{
    NVIC_disable_interrupt_request(this->interrupt_request_number);
}


bool AdcBase::select_module(AdcModule_E module)
{
    this->adc_base_ptr->CFG2 &= ~(ADC_CFG2_MUXSEL_MASK);
    if (module == ADC_MODULE_0) {
        this->adc_base_ptr->CFG2 &= ~(ADC_CFG2_MUXSEL_MASK);  // Select ADC channel A MUX
    }
    else {  // module == ADC_MODULE_1
        this->adc_base_ptr->CFG2 |= ADC_CFG2_MUXSEL_MASK;
    }
    this->selected_module = module;
    return true;
}
