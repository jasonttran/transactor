#include "adc_base.hpp"

#include "mk20d7.h"

typedef enum {
    /* Single ended channels */
    ADC_CHANNEL_8 = 0,  // PTB0
    ADC_CHANNEL_9,  // PTB1
    ADC_CHANNEL_12,  // PTB2
    ADC_CHANNEL_13,  // PTB3
    ADC_CHANNEL_14,  // PTC0
    ADC_CHANNEL_15,  // PTC1
    ADC_NUM_CHANNELS
} AdcChannel_E;


class Adc0 : public AdcBase
{
public:
    static Adc0 * get_instance(void);
    static void isr_handle_hook(void);

protected:
    Adc0();

private:
    virtual void initialize_peripheral(void);
    virtual bool initialize_pin(AdcModule_E module, uint8_t channel);
    virtual ADC_Type * get_adc_base_ptr(void);
    virtual IRQn_Type get_interrupt_request_number(void);
};
