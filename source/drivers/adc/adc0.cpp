#include "adc0.hpp"

#include <stdint.h>
#include <stdlib.h>

typedef struct {
    uint8_t channel;
    uint8_t pin;
    PORT_Type *port_ptr;
} AdcChannelConfig_S;

AdcChannelConfig_S g_adc_config[ADC_NUM_CHANNELS] = {
    [ADC_CHANNEL_8] = {
        .channel = 8,
        .pin = 0,
        .port_ptr = PORTB
    },
    [ADC_CHANNEL_9] = {
        .channel = 9,
        .pin = 1,
        .port_ptr = PORTB
    },
    [ADC_CHANNEL_12] = {
        .channel = 12,
        .pin = 2,
        .port_ptr = PORTB
    },
    [ADC_CHANNEL_13] = {
        .channel = 13,
        .pin = 3,
        .port_ptr = PORTB
    },
    [ADC_CHANNEL_14] = {
        .channel = 14,
        .pin = 0,
        .port_ptr = PORTC
    },
    [ADC_CHANNEL_15] = {
        .channel = 15,
        .pin = 1,
        .port_ptr = PORTC
    }
};


Adc0::Adc0() : AdcBase()
{

}


Adc0 * Adc0::get_instance(void)
{
    static Adc0 *instance = NULL;
    if (instance == NULL) {
        instance = new Adc0;
    }
    return instance;
}


void Adc0::isr_handle_hook(void)
{
    Adc0 *adc = Adc0::get_instance();
    adc->isr_handle();
}


/*
 * Private methods
 */


void Adc0::initialize_peripheral(void)
{
    SIM->SCGC6 |= SIM_SCGC6_ADC0_MASK;
}


bool Adc0::initialize_pin(AdcModule_E module, uint8_t channel)
{
    bool success = false;
    if (module == ADC_MODULE_0) {
        for (uint8_t index = 0; index < ADC_NUM_CHANNELS; index++) {
            if (channel == g_adc_config[index].channel) {
                g_adc_config[index].port_ptr->PCR[g_adc_config[index].pin] &= ~(PORT_PCR_MUX_MASK);
                success = true;
                break;
            }
        }
    }
    return success;
}


ADC_Type * Adc0::get_adc_base_ptr(void)
{
    return ADC0;
}


IRQn_Type Adc0::get_interrupt_request_number(void)
{
    return ADC0_IRQn;
}
