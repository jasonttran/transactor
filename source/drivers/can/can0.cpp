#include "can0.hpp"

#include "mk20d7.h"


Can0::Can0(void) : CanBase()
{

}


Can0* Can0::get_instance(void)
{
    static Can0 *instance = NULL;
    if (instance == NULL) {
        instance = new Can0();
    }
    return instance;
}


void Can0::isr_transaction_hook(void)
{
    Can0 *Can0 = Can0::get_instance();
    Can0->isr_handle_transaction();
}


void Can0::initialize_peripheral(void)
{
    // Enable FlexCAN clock gate
    SIM->SCGC6 |= SIM_SCGC6_FLEXCAN0_MASK;

    // Internal pull-up enabled
    PORTA->PCR[12] |= PORT_PCR_PS_MASK;
    PORTA->PCR[12] |= PORT_PCR_PE_MASK;

    PORTA->PCR[13] |= PORT_PCR_PS_MASK;
    PORTA->PCR[13] |= PORT_PCR_PE_MASK;

    // Select multiplexed signal 
    PORTA->PCR[12] |= PORT_PCR_MUX(2);  // CAN0_TX
    PORTA->PCR[13] |= PORT_PCR_MUX(2);  // CAN0_RX
}


CAN_Type * const Can0::get_can_base_ptr(void)
{
    return CAN0;
}


IRQn_Type Can0::get_interrupt_request_number(void)
{
    return CAN0_ORed_Message_buffer_IRQn;
}
