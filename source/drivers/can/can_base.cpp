#include "can_base.hpp"

#include <stdio.h>
#include <stdlib.h>

#include "mk20d7.h"

#define RX_FIFO_SIZE 8U
#define TX_MB_SIZE 8U
#define RX_MB_INACTIVE 0U
#define TX_MB_INACTIVE 8U
#define TX_MB_SEND_ONCE 12U
#define DEFAULT_RECEIVE_QUEUE_SIZE 128U


CanBase::CanBase(void)
{

}


bool CanBase::initialize(uint32_t baud, uint32_t receive_timeout)
{
    bool success = true;
    can_base_ptr = this->get_can_base_ptr();
    this->receive_timeout = receive_timeout;
    interrupt_request_number = this->get_interrupt_request_number();
    this->receive_queue = xQueueCreate(DEFAULT_RECEIVE_QUEUE_SIZE, sizeof(CanMessage_S));
    this->table_index = 0;

    // Ok, so the schematic says its 16MHz, PTA[18] PTA[19]
    // Turn on Oscillator
    OSC->CR |= OSC_CR_ERCLKEN_MASK;

    // Set to multiplexed peripheral
    initialize_peripheral();

    //  On reset, CAN is disabled
    // Select Oscillator instead of bus clock
    can_base_ptr->CTRL1 &= ~CAN_CTRL1_CLKSRC_MASK;

    // Enable FlexCAN and manually put into Freeze Mode
    can_base_ptr->MCR &= ~CAN_MCR_MDIS_MASK;
    can_base_ptr->MCR |= CAN_MCR_FRZ_MASK;

    // Wait until FlexCAN is out of low power mode
    while (can_base_ptr->MCR & CAN_MCR_LPMACK_MASK);

    // Perform a soft reset
    can_base_ptr->MCR |= CAN_MCR_SOFTRST_MASK;
    while (can_base_ptr->MCR & CAN_MCR_SOFTRST_MASK);

    while (!(can_base_ptr->MCR & CAN_MCR_NOTRDY_MASK));

    // Wait until FlexCAN goes into freeze Mode
    while (!(can_base_ptr->MCR & CAN_MCR_FRZACK_MASK));

#if 0
    // Enable loop-back mode
    can_base_ptr->CTRL1 |= CAN_CTRL1_LPB_MASK;
    can_base_ptr->MCR &= ~CAN_MCR_SRXDIS_MASK;
#else
    // Disable self-reception
    can_base_ptr->MCR |= CAN_MCR_SRXDIS_MASK;
    can_base_ptr->CTRL1 &= ~CAN_CTRL1_LPB_MASK;
#endif

    // Enable Interrupts for the first 16 MBs
    can_base_ptr->IMASK1 |= CAN_IMASK1_BUFLM(0xFFFF);

    // Before enabling RxFIFO, we must clear the interrupt flags associated with it to be safe
    // BUF7I, BUF6I, BUF5I
    // We'll just clear everything
    can_base_ptr->IFLAG1 = 0;

    for (uint8_t index=0; index < RX_FIFO_SIZE; index++) {
        // Set RX MBs as inactive since RX FIFO is used instead
        // BEFORE MCR[RFEN] bit is set or else it will be stuck
        can_base_ptr->MB[index].CS |= CAN_CS_CODE(RX_MB_INACTIVE);
    }

    // Enable RxFIFO
    can_base_ptr->MCR |= CAN_MCR_RFEN_MASK;

    // ID Acceptance mode : Format A
    can_base_ptr->MCR |= CAN_MCR_IDAM(0);

    // Make sure MCR[IRMQ] is negated (0), so we are using RXMGMASK or RXFGMASK instead of individual filters
    // Negating MCR[IRMQ] will not use FIFO filter table
    can_base_ptr->MCR &= ~CAN_MCR_IRMQ_MASK;

    this->set_baud(baud);

    // Set up RXFIFO
    // It uses the first 6 Mailbox/Message Buffer (MB) w/e you want to call it
    // CTRL2[RFFN] at reset value (0) so first 8 MB are used by FIFO and ID filter table
    // Leaving us with only 8 MB for tramsmit
    // See set_filter() to set filter for RXFIFO
    can_base_ptr->CTRL2 |= CAN_CTRL2_RFFN(0);

    // Enable interrupt
    this->initialize_interrupt();

    return success;
}

void CanBase::set_mask(CanFilter_S *mask)
{
    // I'm not sure if this is needed since i am using RXFIFO and not RX Mailboxes
    // This option will only have effect if MCR[IRMQ] bit is negated, which it is by reset value
    // Only can be called in Freeze Mode!
    // If not called, by default, every bit in the filter is checked
    // Use Masking scheme with RXMGMASK, RX14MASK, RX15MASK, RXFGMASK

    can_base_ptr = this->get_can_base_ptr();

    // Check to see if RXFIFO is enabled b/c they use different mask registers
    if (can_base_ptr->MCR & CAN_MCR_RFEN_MASK) {
        // Clear whole register to make sure everything is don't care
        can_base_ptr->RXFGMASK &= ~CAN_RXFGMASK_FGM_MASK;
        if (mask->ide) {
            can_base_ptr->RXFGMASK |= (mask->rtr << 31) | (mask->ide << 30) | ((mask->id & 0x1FFFFFFF) << 1);
        }
        else {
            can_base_ptr->RXFGMASK |= (mask->rtr << 31) | (mask->ide << 30) | (((mask->id & 0x7FF) << 18) << 1);
        }
    }
    else {
        // Clear whole register to make sure everything is don't care
        can_base_ptr->RXMGMASK &= ~CAN_RXMGMASK_MG_MASK;
        if (mask->ide) {
            can_base_ptr->RXMGMASK |= (mask->rtr << 31) | (mask->ide << 30) | ((mask->id & 0x1FFFFFFF));
        }
        else {
            can_base_ptr->RXMGMASK |= (mask->rtr << 31) | (mask->ide << 30) | ((mask->id & 0x7FF) << 18);
        }
    }

}


void CanBase::set_filter(CanFilter_S *filter)
{
    // Filter for RXFIFO
    // There are 8 filters when CTRL2[RFFN] is set to 0
    uint8_t index = this->table_index & 0x7;

    // Check to see if RXFIFO is enabled or else you're modifying MB address instead...
    // (They share same memory address)
    if (can_base_ptr->MCR & CAN_MCR_RFEN_MASK) {
        // Table 44-74
        if (filter->ide) {
            FLEXCAN_IDFLT_TAB(index) = (filter->rtr << 31) | (filter->ide << 30) | ((filter->id & 0x1FFFFFFF) << 1);
        }
        else {
            FLEXCAN_IDFLT_TAB(index) = (filter->rtr << 31) | (filter->ide << 30) | ((filter->id & 0x7FF) << 19);
        }
        // Auto increment the filter table and wrap around if more than 8 are set
        this->table_index++;
    }
}


void CanBase::start(void)
{
    // Make sure set_mask and/or set_filter is set before calling this function!!!

    can_base_ptr = this->get_can_base_ptr();

    // MB 8 - 15 can be used for TX, but first set them to inactive since they are not used
    for (uint8_t index=TX_MB_SIZE; index < TX_MB_SIZE+8; index++) {
        can_base_ptr->MB[index].CS |= CAN_CS_CODE(TX_MB_INACTIVE);
    }

    // Get out of Freeze mode
    can_base_ptr->MCR &= ~CAN_MCR_HALT_MASK;
    can_base_ptr->MCR &= ~CAN_MCR_FRZ_MASK;

    // Wait until it gets out of Freeze mode
    while (can_base_ptr->MCR & CAN_MCR_FRZACK_MASK);

    // Wait until it has entered either Normal, Listen-only, or loop-back mode
    while (can_base_ptr->MCR & CAN_MCR_NOTRDY_MASK);
}


void CanBase::transmit(CanMessage_S *message, uint8_t mailbox_number)
{
    // 8 MBs available for us to use, they are 8-15
    // mailbox number can be between number between 0 - 7
    uint8_t index = (mailbox_number & 0x7) + TX_MB_SIZE;

    can_base_ptr = this->get_can_base_ptr();

    // Zero out DLC bits to prevent garbage
    can_base_ptr->MB[index].CS &= ~(CAN_CS_DLC_MASK | CAN_CS_RTR_MASK | CAN_CS_IDE_MASK);

    // Prepare by setting to inactive first
    can_base_ptr->MB[index].CS |= CAN_CS_CODE(TX_MB_INACTIVE);

    can_base_ptr->MB[index].CS |= (message->rtr << CAN_CS_RTR_SHIFT) | CAN_CS_DLC(message->dlc);

    // I don't want trash ID...
    can_base_ptr->MB[index].ID &= ~(CAN_ID_STD_MASK | CAN_ID_EXT_MASK);

    if (message->ide) {
        // Extended ID
        // SRR and IDE needs to be '1'
        can_base_ptr->MB[index].CS |= CAN_CS_SRR_MASK | CAN_CS_IDE_MASK;
        can_base_ptr->MB[index].ID |= message->id & (CAN_ID_STD_MASK | CAN_ID_EXT_MASK);
    }
    else {
        // Standard ID
        can_base_ptr->MB[index].ID |= CAN_ID_STD(message->id);
    }

    // Zero out data words
    can_base_ptr->MB[index].WORD0 = 0;
    can_base_ptr->MB[index].WORD1 = 0;

    can_base_ptr->MB[index].WORD0 |= CAN_WORD0_DATA_BYTE_3(message->data.field.byte[3]) | CAN_WORD0_DATA_BYTE_2(message->data.field.byte[2]) | CAN_WORD0_DATA_BYTE_1(message->data.field.byte[1]) | CAN_WORD0_DATA_BYTE_0(message->data.field.byte[0]);
    can_base_ptr->MB[index].WORD1 |= CAN_WORD1_DATA_BYTE_7(message->data.field.byte[7]) | CAN_WORD1_DATA_BYTE_6(message->data.field.byte[6]) | CAN_WORD1_DATA_BYTE_5(message->data.field.byte[5]) | CAN_WORD1_DATA_BYTE_4(message->data.field.byte[4]);

    // Last but not least... send it
    can_base_ptr->MB[index].CS |= CAN_CS_CODE(TX_MB_SEND_ONCE);

}


bool CanBase::receive(CanMessage_S *message)
{
    bool success = false;
    success = xQueueReceive(this->receive_queue, message, this->receive_timeout);

    return success;
}


void CanBase::isr_handle_transaction(void)
{
    can_base_ptr = this->get_can_base_ptr();

    // These are Tx MBs (8-15)
    if (can_base_ptr->IFLAG1 & 0xFF00) {
        // We'll just clear them for now
        can_base_ptr->IFLAG1 |= (0xFF << 8);
    }

    // Frame is available in the RXFIFO
    if (can_base_ptr->IFLAG1 & CAN_IFLAG1_BUF5I_MASK) {
        // Save it to a FREERTOS queue
        // Output of RXFIFO is the first MB address
        CanMessage_S message;
        message.srr = (can_base_ptr->MB[0].CS & CAN_CS_SRR_MASK) ? 1 : 0;
        message.ide = (can_base_ptr->MB[0].CS & CAN_CS_IDE_MASK) ? 1 : 0;
        message.dlc = ((can_base_ptr->MB[0].CS & CAN_CS_DLC_MASK) >> CAN_CS_DLC_SHIFT);
        message.ts = (can_base_ptr->MB[0].CS & CAN_CS_TIME_STAMP_MASK);
        if (message.ide) {
            message.id = can_base_ptr->MB[0].ID & (CAN_ID_EXT_MASK | CAN_ID_STD_MASK);
        }
        else {
            message.id = ((can_base_ptr->MB[0].ID & CAN_ID_STD_MASK) >> CAN_ID_STD_SHIFT);
        }
        message.data.field.byte[0] = ((can_base_ptr->MB[0].WORD0 & CAN_WORD0_DATA_BYTE_0_MASK) >> CAN_WORD0_DATA_BYTE_0_SHIFT);
        message.data.field.byte[1] = ((can_base_ptr->MB[0].WORD0 & CAN_WORD0_DATA_BYTE_1_MASK) >> CAN_WORD0_DATA_BYTE_1_SHIFT);
        message.data.field.byte[2] = ((can_base_ptr->MB[0].WORD0 & CAN_WORD0_DATA_BYTE_2_MASK) >> CAN_WORD0_DATA_BYTE_2_SHIFT);
        message.data.field.byte[3] = ((can_base_ptr->MB[0].WORD0 & CAN_WORD0_DATA_BYTE_3_MASK) >> CAN_WORD0_DATA_BYTE_3_SHIFT);
       
        message.data.field.byte[4] = ((can_base_ptr->MB[0].WORD1 & CAN_WORD1_DATA_BYTE_4_MASK) >> CAN_WORD1_DATA_BYTE_4_SHIFT);
        message.data.field.byte[5] = ((can_base_ptr->MB[0].WORD1 & CAN_WORD1_DATA_BYTE_5_MASK) >> CAN_WORD1_DATA_BYTE_5_SHIFT);
        message.data.field.byte[6] = ((can_base_ptr->MB[0].WORD1 & CAN_WORD1_DATA_BYTE_6_MASK) >> CAN_WORD1_DATA_BYTE_6_SHIFT);
        message.data.field.byte[7] = ((can_base_ptr->MB[0].WORD1 & CAN_WORD1_DATA_BYTE_7_MASK) >> CAN_WORD1_DATA_BYTE_7_SHIFT);

        (void)xQueueSendFromISR(this->receive_queue, &message, NULL);

        // Clear the flag so we can get the next item from RXFIFO
        can_base_ptr->IFLAG1 |= CAN_IFLAG1_BUF5I_MASK;
        (void)can_base_ptr->TIMER;
    }

    NVIC_clear_interrupt_request(this->interrupt_request_number);
}


/*
 * Private methods
 */


void CanBase::set_baud(uint32_t baud)
{
    can_base_ptr = this->get_can_base_ptr();

    // http://www.bittiming.can-wiki.info/#Freescale
    // For all your calculations :)
    // All configuration are for Sampling Point at 87.5%
    // All should add up to 16 Time Quanta for the following:
    // PSEG1 += 1
    // PSEG2 += 1
    // PROP_SEG += 1

    // RJW += 1
    // PREDIV += 1

    can_base_ptr->CTRL1 |= CAN_CTRL1_PROPSEG(0x4);  // 5
    can_base_ptr->CTRL1 |= CAN_CTRL1_PSEG2(0x1); // 2
    can_base_ptr->CTRL1 |= CAN_CTRL1_PSEG1(0x7); // 8
    can_base_ptr->CTRL1 |= CAN_CTRL1_RJW(0x0); // 1
    switch (baud) {
        case 50000U:
            can_base_ptr->CTRL1 |= CAN_CTRL1_PRESDIV(0x13);
            break;

        case 100000U:
            can_base_ptr->CTRL1 |= CAN_CTRL1_PRESDIV(0x9);
            break;

        case 125000U:
            can_base_ptr->CTRL1 |= CAN_CTRL1_PRESDIV(0x7);
            break;

        case 250000U:
            can_base_ptr->CTRL1 |= CAN_CTRL1_PRESDIV(0x3);
            break;

        case 500000U:
            can_base_ptr->CTRL1 |= CAN_CTRL1_PRESDIV(0x1);
            break;

        case 1000000U:
            can_base_ptr->CTRL1 |= CAN_CTRL1_PRESDIV(0x0);
            break;

        default:
            can_base_ptr->CTRL1 |= CAN_CTRL1_PRESDIV(0x7); // 125kHz
            break;
    }
}


void CanBase::initialize_interrupt(void)
{
    NVIC_config_interrupt_request_priority(this->interrupt_request_number, DEFAULT_INTERRUPT_PRIORITY_LEVEL);
    NVIC_enable_interrupt_request(this->interrupt_request_number);
}
