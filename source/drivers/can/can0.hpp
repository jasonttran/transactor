#ifndef CAN0_HPP
#define CAN0_HPP

#include "can_base.hpp"


class Can0 : public CanBase
{
public:
    static Can0 * get_instance(void);
    static void isr_transaction_hook(void);

protected:
    Can0();

private:
    virtual void initialize_peripheral(void);
    virtual CAN_Type * const get_can_base_ptr(void);
    virtual IRQn_Type get_interrupt_request_number(void);
};


#endif  // CAN0_HPP
