#ifndef CAN_BASE_HPP
#define CAN_BASE_HPP

#include <stdbool.h>
#include <stdint.h>
#include "FreeRTOS.h"
#include "queue.h"

#include "nvic.h"

typedef struct {
    uint8_t rtr : 1;
    uint8_t ide : 1;
    uint32_t id;
} CanFilter_S;

typedef struct {
    union {
        uint64_t all;
        struct {
            uint8_t byte[8];
            uint16_t word[4];
            uint32_t dword[2];
        } field;
    };
} CanMessageData_S;

typedef struct {
    uint32_t            : 4;   // Reserved
    uint32_t code       : 4;   // Used for buffer matching and arbitration process
    uint32_t            : 1;   // Reserved
    uint32_t srr        : 1;   // Only used in extended format, should be '1' (Recessive) for TX
    uint32_t ide        : 1;   // Whether frame is standard or extended
    uint32_t rtr        : 1;   // Remote Transmit Request
    uint32_t dlc        : 4;   // Data Length Code, length in bytes
    uint32_t ts         : 16;  // Time Stamp
    uint32_t priority   : 3;   // Only used when LPRIO_EN bit is set in MCR, not transmitted
    uint32_t id         : 29;  // ID of message
    CanMessageData_S data;     // Data to be transmitted
} __attribute__((__packed__)) CanMessage_S;


class CanBase
{
public:
    bool initialize(uint32_t baud, uint32_t receive_timeout);
    void start(void);
    void transmit(CanMessage_S *message, uint8_t mailbox_number);
    bool receive(CanMessage_S *message);
    void set_filter(CanFilter_S *filter);
    void set_mask(CanFilter_S *mask);

    /* Interrupts */
    void isr_handle_transaction(void);

protected:
    CanBase();

private:
    virtual void initialize_peripheral(void) = 0;
    virtual CAN_Type * const get_can_base_ptr(void) = 0;
    virtual IRQn_Type get_interrupt_request_number(void) = 0;

    void initialize_interrupt(void);
    void set_baud(uint32_t baud);

    uint32_t receive_timeout;
    CAN_Type *can_base_ptr;
    IRQn_Type interrupt_request_number;
    QueueHandle_t receive_queue;

    uint8_t table_index;  // index for filter
};


#endif  // CAN_BASE_HPP
