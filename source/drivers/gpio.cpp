#include "gpio.hpp"

#include <stddef.h>


Gpio::Gpio(GpioPort_E port, uint8_t pin) : pin(pin)
{
    PORT_Type *port_base_ptr = NULL;
    this->gpio_base_ptr = NULL;

    switch (port) {
        case GPIO_PORT_A:
        {
            SIM->SCGC5 |= SIM_SCGC5_PORTA_MASK;
            port_base_ptr = PORTA;
            this->gpio_base_ptr = PTA;
            break;
        }
        case GPIO_PORT_B:
        {
            SIM->SCGC5 |= SIM_SCGC5_PORTB_MASK;
            port_base_ptr = PORTB;
            this->gpio_base_ptr = PTB;
            break;
        }
        case GPIO_PORT_C:
        {
            SIM->SCGC5 |= SIM_SCGC5_PORTC_MASK;
            port_base_ptr = PORTC;
            this->gpio_base_ptr = PTC;
            break;
        }
        case GPIO_PORT_D:
        {
            SIM->SCGC5 |= SIM_SCGC5_PORTD_MASK;
            port_base_ptr = PORTD;
            this->gpio_base_ptr = PTD;
            break;
        }
        case GPIO_PORT_E:
        {
            SIM->SCGC5 |= SIM_SCGC5_PORTE_MASK;
            port_base_ptr = PORTE;
            this->gpio_base_ptr = PTE;
            break;
        }
        default:
        {
            break;
        }
    }

    port_base_ptr->PCR[this->pin] |= PORT_PCR_MUX(1);
}


void Gpio::config_output(void)
{
    this->gpio_base_ptr->PDDR |= (1 << this->pin);
}


void Gpio::config_input(void)
{
    this->gpio_base_ptr->PDDR &= ~(1 << this->pin);
}


void Gpio::set_high(void)
{
    this->gpio_base_ptr->PSOR |= (1 << this->pin);
}


void Gpio::set_low(void)
{
    this->gpio_base_ptr->PCOR |= (1 << this->pin);
}


void Gpio::toggle(void)
{
    this->gpio_base_ptr->PTOR |= (1 << this->pin);
}


bool Gpio::read(void)
{
    return this->gpio_base_ptr->PDIR & (1 << this->pin);
}
