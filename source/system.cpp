#include "system.hpp"

#include <stdbool.h>

#include "gpio.hpp"
#include "mk20d7.h"

// Externally defined clock frequencies
extern uint32_t _system_clock_mhz;
extern uint32_t _bus_clock_mhz;
extern uint32_t _flash_clock_mhz;


uint32_t SYSTEM_get_system_clock_mhz(void)
{
    return _system_clock_mhz;
}


uint32_t SYSTEM_get_perpherial_clock_mhz(void)
{
    return _bus_clock_mhz;
}


uint32_t SYSTEM_get_flash_clock_mhz(void)
{
    return _flash_clock_mhz;
}


void SYSTEM_reset(void)
{
    WDOG->UNLOCK = 0xC520;
    WDOG->UNLOCK = 0xD928;
    WDOG->STCTRLH |= WDOG_STCTRLH_WDOGEN_MASK;
    while (true) {
    }
}


void SYSTEM_error_loop(void)
{
    Gpio led(GPIO_PORT_C, 5);
    led.config_output();
    led.set_low();
    while (true) {
        for (uint32_t counter = 0U; counter < 1000000; counter++) {
            // Do nothing
        }
        led.toggle();
    }
}
