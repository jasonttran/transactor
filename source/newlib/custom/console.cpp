#include "console.hpp"

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include "FreeRTOS.h"

#include "uart0.hpp"


void CONSOLE_init(void)
{
    Uart0 *uart = Uart0::get_instance();
    uart->initialize(CONSOLE_SERIAL_BAUD, portMAX_DELAY, portMAX_DELAY);
}


void CONSOLE_write(char *buffer, int length)
{
    Uart0 *uart = Uart0::get_instance();
    if (buffer != NULL) {
        (void) uart->send_buffer((uint8_t *)buffer, length);
    }
}


void CONSOLE_flush(void)
{
    fflush(stdout);
}
