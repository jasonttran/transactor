#ifndef CONSOLE_HPP
#define CONSOLE_HPP


#define CONSOLE_SERIAL_BAUD 9600U


void CONSOLE_init(void);
void CONSOLE_write(char *buffer, int length);
void CONSOLE_flush(void);


#endif  // CONSOLE_HPP
