#include "syscall.hpp"

#include <errno.h>
#include <stdio.h>

#include "console.hpp"

extern char _bheap;
extern char _eheap;

typedef struct {
    char *next_heap_ptr;           ///< The next pointer _sbrk() may provide
    unsigned int sbrk_calls;       ///< Number of calls to _sbrk()
    unsigned int last_sbrk_size;   ///< The last size requested from _sbrk()
    void *last_sbrk_ptr;           ///< The last pointer given by _sbrk()
} SyscallMemory_S;

static SyscallMemory_S syscall_memory = {0};


void SYSCALL_init(void)
{
    CONSOLE_init();
}


/*
 * Newlib system call hooks
 */

void * _sbrk(size_t requested_bytes)
{
    char *ret = NULL;

    if (syscall_memory.next_heap_ptr == NULL) {
        syscall_memory.next_heap_ptr = (char *)&_bheap;
    }

    ret = syscall_memory.next_heap_ptr;
    syscall_memory.next_heap_ptr += requested_bytes;

    syscall_memory.sbrk_calls++;
    syscall_memory.last_sbrk_size = requested_bytes;
    syscall_memory.last_sbrk_ptr = ret;

    return ret;
}


int _write(int file, char *buf, int nbytes)
{
    error_t error = EINVAL;
    if (buf != NULL) {
        if (file == fileno(stdout)) {
            error = 0;
            CONSOLE_write(buf, nbytes);
        }
    }
    return -error;
}
