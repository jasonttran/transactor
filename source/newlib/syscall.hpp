#ifndef SYSCALL_HPP
#define SYSCALL_HPP

#include <stddef.h>
#include <sys/stat.h>

#if __cplusplus
extern "C" {
#endif  // __cplusplus


void SYSCALL_init(void);


/*
 *  Newlib system call hooks
 */
void * _sbrk(size_t requested_bytes);
int _write(int file, char *buf, int nbytes);


#if __cplusplus
}
#endif  // __cplusplus

#endif  // SYSCALL_HPP
