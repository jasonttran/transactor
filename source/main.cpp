#include "FreeRTOS.h"
#include "task.h"

#include "heartbeat.hpp"
#include "system.hpp"

static void application_create_tasks(void);


int main(void)
{
    application_create_tasks();
    vTaskStartScheduler();
    SYSTEM_error_loop();
    return -1;
}


static void application_create_tasks(void)
{
    xTaskCreate(HEARTBEAT_task, "HEARTBEAT_task", 512, NULL, PRIORITY_HIGH, NULL);
}
