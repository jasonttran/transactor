/*
    Note:
        - Core and system clock are from the same source
        - System clock is distributed to the crossbar switch, the bus masters directly connected to the crossbar, and UART0 / UART1
 */

#ifndef SYSTEM_H
#define SYSTEM_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif  // __cplusplus


uint32_t SYSTEM_get_system_clock_mhz(void);
uint32_t SYSTEM_get_perpherial_clock_mhz(void);
uint32_t SYSTEM_get_flash_clock_mhz(void);
void SYSTEM_reset(void);
void SYSTEM_error_loop(void);


#ifdef __cplusplus
}
#endif  // __cplusplus

#endif  // SYSTEM_H
