/*
    References:
        - http://www.bravegnu.org/gnu-eprog/data-in-ram.html
        - https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/4/html/Using_ld_the_GNU_Linker/expressions.html
        - https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/4/html/Using_ld_the_GNU_Linker/sections.html#OUTPUT-SECTION-LMA
        - https://community.nxp.com/thread/306667
        - https://www.nxp.com/docs/en/quick-reference-guide/KQRUG.pdf
 */

#include "mk20d7.h"

#include <stdint.h>

#include "syscall.hpp"
#include "gpio.hpp"

// Clock frequencies are configured by init_clock
#define CORE_CLOCK_MHZ 48000000UL

// Clock frequencies are configured by init_clock
uint32_t _system_clock_mhz = CORE_CLOCK_MHZ;
uint32_t _bus_clock_mhz = CORE_CLOCK_MHZ;
uint32_t _flash_clock_mhz = CORE_CLOCK_MHZ / 2U;

// RAM addresses
extern char _bdata;
extern char _edata;
extern char _bdata_lma;
extern char _bdata_vma;
extern char _data_size;
extern char _bbss;
extern char _ebss;
extern char _bss_size;

#if __cplusplus
    extern "C" {
#endif  // __cplusplus

extern void _start(void);
extern int main(void);

#if __cplusplus
}
#endif  // __cplusplus

// Watchdog initialization
static void init_watchdog(void);

// Clock initialization
static void init_clock(void);

// RAM initialization
static void init_ram(void);
static void init_ram_config(void);
static void init_ram_data(void);
static void init_ram_bss(void);

// Port initialization
static void init_ports(void);


void _start(void)  // Entry point
{
    init_watchdog();
    init_clock();
    init_ram();
    init_ports();
    SYSCALL_init();
    main();
}


static void init_watchdog(void)
{
    WDOG->UNLOCK = 0xC520;
    WDOG->UNLOCK = 0xD928;
    WDOG->STCTRLH &= ~(WDOG_STCTRLH_WDOGEN_MASK);
}


static void init_clock(void)
{
    // OSC (Oscillator) module controls internal and external crystal oscillators
    // MCG (Multipurpose Clock Generator) module controls clock source through multiplexing
    // SIM (System Integration Module) module controls clock division and gating

    // Enable external oscillator output to MCG
    OSC->CR = OSC_CR_ERCLKEN_MASK;

    // Enable external clock reference
    // Configure high frequency range for crystal oscillator
    // Configure high gain oscillator
    // Enable external clock reference from Oscillator module (OSC)
    MCG->C2 = MCG_C2_RANGE0(1) | MCG_C2_HGO0_MASK | MCG_C2_EREFS0_MASK;

    // Configure FLL reference clock divider to keep it within the valid range (despite the fact that the FLL is not used)
    // Temporarily switch MCG output clock source to external reference clock
    // 'b011: If RANGE 0 = 0 or OSCSEL=1, Divide Factor is 8; for all other RANGE 0 values, Divide Factor is 256
    MCG->C1 = MCG_C1_CLKS(2) | MCG_C1_FRDIV(3);

    // Poll MCG status
    while (!(MCG->S & MCG_S_OSCINIT0_MASK));  // Wait for oscillator to initialize
    while (MCG->S & MCG_S_IREFST_MASK);  // Wait for Reference clock to switch to external reference
    while (((MCG->S & MCG_S_CLKST_MASK) >> MCG_S_CLKST_SHIFT) != 0x2);  // Wait for MCGOUT to switch over to the external reference clock

    // Assuming external oscillator is driven by a 16 MHz external crystal
    // Note: PLL reference clock must be in range 2 - 4 MHz
    MCG->C5 |= MCG_C5_PRDIV0(7);  // Configure PLL reference divider = 8; therefore, PLL reference clock = 16 MHz / 8 = 2 MHz
    MCG->C6 |= MCG_C6_VDIV0(0);  // VCO0 Divider multiplication factor = 24; therefore, PLL output clock = 2 MHz * 24 = 48 MHz
    MCG->C6 |= MCG_C6_CME0_MASK;  // Enable clock monitor
    MCG->C6 |= MCG_C6_PLLS_MASK;  // Select PLL output

    while (!(MCG->S & MCG_S_PLLST_MASK));
    while (!(MCG->S & MCG_S_LOCK0_MASK));

    // Configure SIM clock dividers before switching MCG clock output to PLL to ensure the system clock speeds comply with specifications
    SIM->CLKDIV1 &= ~(SIM_CLKDIV1_OUTDIV1_MASK);
    SIM->CLKDIV1 &= ~(SIM_CLKDIV1_OUTDIV2_MASK);
    SIM->CLKDIV1 &= ~(SIM_CLKDIV1_OUTDIV4_MASK);
    SIM->CLKDIV1 |= SIM_CLKDIV1_OUTDIV1(0);  // Core = PLL/1 (48 MHz)
    SIM->CLKDIV1 |= SIM_CLKDIV1_OUTDIV2(1);  // Bus = PLL/2 (24 MHz)
    SIM->CLKDIV1 |= SIM_CLKDIV1_OUTDIV4(1);  // Flash = PLL/2 (24 MHz)

    MCG->C1 &= ~(MCG_C1_CLKS_MASK);  // Switch MCG output clock source to PLL output clock
    while (((MCG->S & MCG_S_CLKST_MASK) >> MCG_S_CLKST_SHIFT) != 0x3);

    // The USB clock divider in the System Clock Divider Register 2 (SIM_CLKDIV2) should be configured to generate the 48 MHz USB clock before configuring the USB module
    SIM->CLKDIV2 |= SIM_CLKDIV2_USBDIV(0); // Connfigure USB divider to 1
}


static void init_ram(void)
{
    init_ram_config();
    init_ram_data();
    init_ram_bss();
}


static void init_ram_config(void)
{
    SIM->SOPT1 |= SIM_SOPT1_RAMSIZE(7);  // Configure RAM size 64KB
    SIM->FCFG1 |= SIM_FCFG1_PFSIZE(9);  // Configure program FLASH size 256KB
}


static void init_ram_data(void)
{
    char *src = &_bdata_lma;
    char *dest = &_bdata_vma;
    char *end = &_edata;
    while (dest < end) {
        *dest++ = *src++;
    }
}


static void init_ram_bss(void)
{
    char *ptr = &_bbss;
    char *end = &_ebss;
    while (ptr < end) {
        *ptr++ = 0;
    }
}


static void init_ports(void)
{
    SIM->SCGC5 |= (SIM_SCGC5_PORTA_MASK);
    SIM->SCGC5 |= (SIM_SCGC5_PORTB_MASK);
    SIM->SCGC5 |= (SIM_SCGC5_PORTC_MASK);
    SIM->SCGC5 |= (SIM_SCGC5_PORTD_MASK);
    SIM->SCGC5 |= (SIM_SCGC5_PORTE_MASK);
}
