"""
SCons command line interface
"""

from SCons.Script import *

__author__ = "jtran"
__version__ = "1.1.0"


def cli_init():
    AddOption(
        "--ut", "--unittest",
        dest="unittest",
        action="store_true",
        default=False
    )
    AddOption(
        "--verbose",
        dest="verbose",
        action="store_true",
        default=False
    )
