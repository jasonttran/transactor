"""
SCons site init - executes before SConstruct and SConscripts
"""

from cli import cli_init
from SCons.Script import *

__author__ = "jtran"
__version__ = "1.0.0"


cli_init()
