"""
Project-specific subsidary SConscript
"""

import os

from fsops import scan_tree, filter_files, ch_filename_ext
from osops import is_windows, is_linux

__author__ = "jtran"
__version__ = "1.0.1"


"""
CLI arguments
"""
ENABLE_UNITTEST = GetOption("unittest")
ENABLE_VERBOSE = GetOption("verbose")

"""
Define naming conventions
"""
TARGET_NAME = Dir(".").name


"""
Define file nodes and directory nodes
"""
PROJECT_DIR = Dir(".")
SOURCE_DIR = PROJECT_DIR.Dir("source")
VARIANT_DIR = PROJECT_DIR.Dir("build")
OBJ_DIR = VARIANT_DIR.Dir("obj")
MAP_FILE = VARIANT_DIR.File("{}.map".format(TARGET_NAME))
CMSIS_DIR = Dir("cmsis")

UNITTEST_DIR = PROJECT_DIR.Dir("test")
UNITTEST_BUILD_DIR = VARIANT_DIR.Dir("test")
UNITTEST_SOURCE_DIR = UNITTEST_DIR.Dir("source")
UNITTEST_MOCK_DIR = UNITTEST_DIR.Dir("mock")

INCLUDE_DIRS = [
    CMSIS_DIR.Dir("Core/Include"),
]

INCLUDE_DIRS_ROOT = [
    SOURCE_DIR
]

SRC_DIRS = [
    SOURCE_DIR
]

STARTUP_FILES = [
    SOURCE_DIR.File("startup.s"),
]

LINKER_FILES = [
    SOURCE_DIR.File("layout.ld"),
]

EXCLUDED_SRC_FILES = [

]


"""
Define build environments
"""
Import("arm_env")

arm_env.VariantDir(variant_dir=VARIANT_DIR, src_dir=Dir("."), duplicate=0)

arm_env["CPPDEFINES"].extend([
])

arm_env["LINKFLAGS"].extend([
    "-Wl,-Map,{}".format(MAP_FILE.abspath),
])


"""
Search and group build files
"""

""" Search and group source files and source directories """
target_src_filenodes = []
target_src_dirnodes = []
for dir in SRC_DIRS:
    src_filenodes, src_dirnodes, _, _ = scan_tree(dir)
    target_src_filenodes.extend(src_filenodes)
    target_src_dirnodes.extend(src_dirnodes)

target_src_filenodes.extend(STARTUP_FILES)

""" Search and group linker scripts """
for linker_file in LINKER_FILES:
    arm_env["LINKFLAGS"].append("-T{}".format(File(linker_file).abspath))

""" Search and group include paths """
arm_env["CPPPATH"].extend(INCLUDE_DIRS)
for dir in INCLUDE_DIRS_ROOT:
    _, _, _, include_dirnodes = scan_tree(dir)
    arm_env["CPPPATH"].extend(include_dirnodes)

""" Filter build files """
target_src_filenodes = filter_files(target_src_filenodes, EXCLUDED_SRC_FILES)


"""
Perform builds
"""
obj_filenodes = []
for src_filenode in target_src_filenodes:
    new_filename = ch_filename_ext(src_filenode, "o")
    dest_filepath = OBJ_DIR.File(new_filename.name)
    new_obj_filenodes = arm_env.Object(target=dest_filepath, source=src_filenode)
    obj_filenodes.extend(new_obj_filenodes)

elf_filenodes = arm_env.Program(target=VARIANT_DIR.File("{}.elf".format(TARGET_NAME)), source=obj_filenodes)
bin_filenodes = arm_env.Objcopy(target=VARIANT_DIR.File("{}.bin".format(TARGET_NAME)), source=elf_filenodes)
hex_filenodes = arm_env.Objcopy(target=VARIANT_DIR.File("{}.hex".format(TARGET_NAME)), source=elf_filenodes)
lst_filenodes = arm_env.Objdump(target=VARIANT_DIR.File("{}.lst".format(TARGET_NAME)), source=elf_filenodes)
arm_env.Size(elf_filenodes)

Depends(elf_filenodes, LINKER_FILES)


"""
Unit test
"""
if ENABLE_UNITTEST:
    Import("unittest_env")
    unittest_env["CPPPATH"] += arm_env["CPPPATH"]
    unittest_env["CPPDEFINES"] += arm_env["CPPDEFINES"]

    ut_exes, ut_objs, ut_mock_objs = unittest_env.Build(
        target_dir=UNITTEST_BUILD_DIR,
        sources=[UNITTEST_SOURCE_DIR],
    )

    results = unittest_env.RunBatch(ut_exes, verbose=ENABLE_VERBOSE)
