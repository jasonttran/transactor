"""
https://www.pjrc.com/teensy/loader_cli.html
"""

import argparse
import logging
import os
import subprocess
import sys

logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)

__author__ = "jtran"
__version__ = "1.0.0"

FAILURE = r"""
_____ _    ___ _    _   _ ____  _____
|  ___/ \  |_ _| |  | | | |  _ \| ____|
| |_ / _ \  | || |  | | | | |_) |  _|
|  _/ ___ \ | || |__| |_| |  _ <| |___
|_|/_/   \_\___|_____\___/|_| \_\_____|
"""

SUCCESS = r"""
 ____  _   _  ____ ____ _____ ____ ____
/ ___|| | | |/ ___/ ___| ____/ ___/ ___|
\___ \| | | | |  | |   |  _| \___ \___ \
 ___) | |_| | |__| |___| |___ ___) |__) |
|____/ \___/ \____\____|_____|____/____/
"""

SELF_DIRPATH = os.path.dirname(__file__)
TEENSY_LOADER_EXE = os.path.join(SELF_DIRPATH, "loader", "teensy_loader_cli.exe")


def get_args():
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument("--hex",
                            type=str,
                            metavar="<file path>",
                            default=os.path.join(SELF_DIRPATH, "build", "transactor.hex"),
                            help="Hex filepath")
    arg_parser.add_argument("--mcu",
                            type=str,
                            metavar="<str>",
                            default="mk20dx256")
    return arg_parser.parse_args()


def main():
    args = get_args()
    hex_filepath = args.hex
    mcu = args.mcu

    if not os.path.isfile(hex_filepath):
        logging.error("Unable to find: %s", hex_filepath)
        return 1  # Return early

    cmd = [
        TEENSY_LOADER_EXE,
        "-mmcu={}".format(mcu),
        "-v",
        hex_filepath,
    ]
    logging.debug("Using command: %s", " ".join(cmd))
    error = subprocess.call(cmd, shell=True)
    if error:
        logging.error("Failed to flash target!")

    return error


if __name__ == "__main__":
    _error = main()
    if not _error:
        logging.info(SUCCESS)
    else:
        logging.info(FAILURE)
    sys.exit(_error)
